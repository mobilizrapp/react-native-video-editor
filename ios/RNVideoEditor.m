
#import "RNVideoEditor.h"
#import <UIKit/UIKit.h>
#import <React/RCTConvert.h>
#import <AVFoundation/AVAudioPlayer.h>

#define degreesToRadians( degrees ) ( ( degrees ) / 180.0 * M_PI )

@implementation RNVideoEditor

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}
RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(merge:(NSArray *)videos // array of video details
                  music:(NSString *)music // path to mp3 file
                  musicStart:(double)musicStart //Start time in milliseconds
                  showOverlay:(BOOL *)showOverlay // Show overlay?
                  destination:(NSString *)destination // Destination for export - Instagram or Other
                  errorCallback:(RCTResponseSenderBlock)failureCallback
                  callback:(RCTResponseSenderBlock)successCallback) {
    
    NSLog(@"%@ %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    NSLog(@"videos: %@", videos );
    NSLog(@"music: %@", music );
    NSLog(@"musicStart: %G", musicStart );
    
    [self MergeVideo:videos music:music musicStart:musicStart showOverlay:showOverlay destination:destination callback:successCallback errorCallback:failureCallback];
}

RCT_EXPORT_METHOD(exportVideo:(NSArray *)videos // path to the video
                  destination: (NSString *)destination // Instagram or Other
                  errorCallback:(RCTResponseSenderBlock)failureCallback
                  callback:(RCTResponseSenderBlock)successCallback) {
    
    NSLog(@"%@ %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd));
    
    [self ExportVideo:videos destination:destination callback:successCallback errorCallback:failureCallback];
    
}

-(void)MergeVideo:(NSArray *)videos music:(NSString *)music musicStart:(double)musicStart showOverlay:(BOOL *)showOverlay destination:(NSString *)destination  callback:(RCTResponseSenderBlock)successCallback errorCallback:(RCTResponseSenderBlock)failureCallback
{
    
    CGFloat totalDuration;
    totalDuration = 0;
    
    AVMutableComposition *mixComposition = [[AVMutableComposition alloc] init];
    
    AVMutableCompositionTrack *videoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo
                                                                        preferredTrackID:kCMPersistentTrackID_Invalid];
    
    AVMutableCompositionTrack *audioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio
                                                                        preferredTrackID:kCMPersistentTrackID_Invalid];
    
    int offset = 250;
    CMTime videoStart = CMTimeMake(offset, 1000);
    
    CMTimeRange timeRange;
    CMTime insertTime = kCMTimeZero;
    CMTime subtractor = CMTimeMake(4, 30);
    BOOL hasMusic = ![music isEqualToString:@"none"];
    NSLog(@"hasMusic: %@", hasMusic ? @"Yes" : @"No" );
    BOOL partHasAudio = 0;
    int i = 0;
    CMTime assetDuration;
    
    NSString *path;
    NSString *mode;
    NSString *createPathway;
    
    NSLog(@"videos: %@", videos );
    if (![videos isKindOfClass:[NSArray class]]) {
        failureCallback(@[@"Videos must be passed as an array"]);
        return;
    }
    
    for (id video in videos )
    {
        // we've been sent extra details about the video
        // Check the integrity of the data
        
        if (![video isKindOfClass:[NSArray class]]) {
            failureCallback(@[@"Videos must be passed as an array of arrays"]);
            return;
        }
        
        if (![video[0] isKindOfClass:[NSString class]]) {
            failureCallback(@[@"Video path must be passed as string on %@", video[0] ]);
            return;
        }
        
        if (![video[1] isKindOfClass:[NSString class]]) {
            failureCallback(@[@"Video mode must be passed as string on %@", video[1] ]);
            return;
        }
        
        if (![video[2] isKindOfClass:[NSString class]]) {
            failureCallback(@[@"Video createPathway must be passed as string on %@", video[2] ]);
            return;
        }
        
        path = video[0];
        mode = video[1];
        createPathway = video[2];
        
        // Check is a video exists at the path
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if (![fileManager fileExistsAtPath:path]){
            // Fail! We can't merge videos files that don't exist or we'll crash
            failureCallback(@[@"One or more video files missing"]);
            return;
        }
        
        BOOL hasAudio = 0;
        BOOL hasVideo = 0;
        AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:path]];
        
        // For timelapse clips we have to delete 4 frames from the end due to a bug in RCTCamera if sound is on
        if ([mode isEqualToString:@"timelapse"]) {
            assetDuration = CMTimeSubtract(asset.duration, subtractor);
        } else {
            assetDuration = asset.duration;
        }
        
        if (i == 0 && hasMusic && ( 1 == CMTimeCompare(assetDuration,videoStart))) {
            assetDuration = CMTimeSubtract(assetDuration, videoStart) ;
            timeRange = CMTimeRangeMake(videoStart, assetDuration);
        } else {
            timeRange = CMTimeRangeMake(kCMTimeZero, assetDuration);
        }
        
        // Does this video file actually have video data in it?
        hasVideo = [asset tracksWithMediaType:AVMediaTypeVideo].count;
        if (!hasVideo) {
            failureCallback(@[@"One or more video files corrupted"]);
            return;
        }
        [videoTrack insertTimeRange:timeRange
                            ofTrack:[[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0]
                             atTime:insertTime
                              error:nil];
        
        // if no music track use the video's sound (if it has an audio track)
        hasAudio = [asset tracksWithMediaType:AVMediaTypeAudio].count;
        if (!hasMusic && hasAudio) {
            partHasAudio = 1;
            [audioTrack insertTimeRange:timeRange
                                ofTrack:[[asset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0]
                                 atTime:insertTime
                                  error:nil];
        }
        
        insertTime = CMTimeAdd(insertTime,assetDuration);
        i++;
    }
    
    CGAffineTransform rotationTransform = CGAffineTransformMakeRotation(M_PI_2);
    videoTrack.preferredTransform = rotationTransform;
    
    NSString *documentsDirectory= [self applicationDocumentsDirectory];
    NSString *myDocumentPath = [documentsDirectory stringByAppendingPathComponent:@"merged_video.mp4"];
    NSURL *urlVideoMain = [[NSURL alloc] initFileURLWithPath: myDocumentPath];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:myDocumentPath]) {
        [[NSFileManager defaultManager] removeItemAtPath:myDocumentPath error:nil];
    }
    
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPreset1280x720];
    
    // Show an Overlay
    NSLog(@"showOverlay: %@", [destination isEqualToString:@"Instagram"] || [destination isEqualToString:@"Other"] ? @"YES" : @"NO" );
    if ([destination isEqualToString:@"Instagram"] || [destination isEqualToString:@"Other"]) {
        // get the size of the mixComposition
        CGSize size = CGSizeMake(720, 1280);
        
        // Get the overlay image
        UIImage *overlay = [UIImage imageNamed:@"Hoopy-Watermark-Sept-2018"];
        CGFloat overlayWidth = overlay.size.width;
        CGFloat overlayHeight = overlay.size.height;
        CGFloat overlayRatio = overlayHeight/overlayWidth ;
        CGFloat overlayDisplayWidth = 190;
        CGFloat overlayDisplayHeight = overlayDisplayWidth * overlayRatio;
        CGFloat displayX = size.width - overlayDisplayWidth - 20;
        CGFloat displayY = 0;
        if ([destination isEqualToString:@"Instagram"])  {
            displayY = 290 ;
        } else if ([destination isEqualToString:@"Other"]) {
            displayY = 40 ;
        }
        
        CALayer *parentLayer = [CALayer layer];
        parentLayer.frame = CGRectMake(0, 0, size.width, size.height);
        
        CALayer *videoLayer = [CALayer layer];
        videoLayer.frame = CGRectMake(0, 0, size.width, size.height);
        
        CALayer *overLayer = [CALayer layer];
        [overLayer setContents:(id)[overlay CGImage]];
        overLayer.frame = CGRectMake(displayX, displayY, overlayDisplayWidth, overlayDisplayHeight);
        overLayer.opacity = 0.5;
        [overLayer setMasksToBounds:YES];
        
        [parentLayer addSublayer:videoLayer];
        [parentLayer addSublayer:overLayer];
        
        AVMutableVideoComposition *videoComp = [AVMutableVideoComposition videoComposition];
        videoComp.renderSize = size;
        videoComp.frameDuration = CMTimeMake(1, 30);
        videoComp.animationTool = [AVVideoCompositionCoreAnimationTool
                                   videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer
                                   inLayer:parentLayer];
        
        AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
        instruction.timeRange = CMTimeRangeMake(kCMTimeZero, mixComposition.duration);
        
        AVAssetTrack *videoOverlayTrack = [[mixComposition tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
        AVMutableVideoCompositionLayerInstruction* layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoOverlayTrack];
        
        CGAffineTransform t1 = CGAffineTransformMakeTranslation(720, 0);
        CGAffineTransform t2 = CGAffineTransformRotate(t1, degreesToRadians(90.0));
        [layerInstruction setTransform:t2 atTime:kCMTimeZero];
        instruction.layerInstructions = [NSArray arrayWithObject:layerInstruction];
        videoComp.instructions = [NSArray arrayWithObject: instruction];
        
        exporter.videoComposition = videoComp;
    }
    
    // Replace audio if music has been passed
    if (hasMusic) {
        NSURL *musicUrl ;
        
        // bundled or ipod audio
        if ([music rangeOfString:@"ipod"].location > 0) {
            //          NSString *path = [[NSBundle mainBundle] pathForResource:music ofType:nil];
            musicUrl = [NSURL fileURLWithPath:music];
        } else {
            musicUrl = [NSURL URLWithString:music];
        }
        NSLog(@"musicUrl: %@", musicUrl );
        
        // We have to load the ipod track into an audio player to test if the file exists
        // as we can't get the absolute URL. Thanks Appple
        if (![[AVAudioPlayer alloc] initWithContentsOfURL:musicUrl error:nil]) {
            failureCallback(@[@"Music no longer exists!"]);
            return;
        }
        
        //        // Check if file exists on disk
        //        BOOL musicFileExists = [[NSFileManager defaultManager] fileExistsAtPath:musicUrl.absoluteString];
        //        NSLog(@"musicFileExists: %@", musicFileExists ? @"Yes" : @"No" );
        //
        //        if (!musicFileExists) {
        //            failureCallback(@[@"Music file is missing"]);
        //            return;
        //        }
        
        AVAsset *musicAsset = [AVURLAsset URLAssetWithURL:musicUrl options:nil];
        // Start time is 1/8 second late to allow for video synchronisation
        int startIntVal = (int)musicStart;
        CMTime musicStartCM = CMTimeMake(startIntVal, 1000);
        
        // CMTime videoDuration = CMTimeConvertScale(mixComposition.duration, 1000, 4);
        CMTime videoDuration = mixComposition.duration;
        CMTimeRange musicTimeRange = CMTimeRangeMake(musicStartCM, videoDuration);
        [audioTrack insertTimeRange:musicTimeRange
                            ofTrack:[[musicAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0]
                             atTime:kCMTimeZero
                              error:nil];
        
        // add short fade in and fade out to avoid clicks
        CMTime fadeTime = CMTimeMake(50, 1000);
        CMTime startFadeInTime = musicStartCM;
        CMTime endFadeInTime = CMTimeAdd(fadeTime,musicStartCM);
        CMTime startFadeOutTime =  CMTimeSubtract(videoDuration, fadeTime);
        CMTime endFadeOutTime = videoDuration;
        
        AVMutableAudioMix *mutableAudioMix = [AVMutableAudioMix audioMix];
        AVMutableAudioMixInputParameters *mixParameters = [AVMutableAudioMixInputParameters audioMixInputParametersWithTrack:audioTrack];
        CMTimeRange fadeInTimeRange = CMTimeRangeFromTimeToTime(startFadeInTime, endFadeInTime);
        CMTimeRange fadeOutTimeRange = CMTimeRangeFromTimeToTime(startFadeOutTime, endFadeOutTime);
        [mixParameters setVolumeRampFromStartVolume:0.0 toEndVolume:1.0 timeRange:fadeInTimeRange];
        [mixParameters setVolumeRampFromStartVolume:1.0 toEndVolume:0.0 timeRange:fadeOutTimeRange];
        mutableAudioMix.inputParameters = @[mixParameters];
        exporter.audioMix = mutableAudioMix;
    }
    
    // if we have no audio at all we need to remove our audio track otherwise export fails
    if (!partHasAudio && !hasMusic) {
        [mixComposition removeTrack:audioTrack];
    }
    
    exporter.outputURL = urlVideoMain;
    exporter.outputFileType = @"com.apple.quicktime-movie";
    exporter.shouldOptimizeForNetworkUse = YES;
    
    [exporter exportAsynchronouslyWithCompletionHandler:^{
        
        switch ([exporter status]) {
            case AVAssetExportSessionStatusFailed:
                NSLog(@"AVAssetExportSessionStatusFailed %@", exporter.error);
                failureCallback(@[@"Merge video AVAssetExportSessionStatusFailed", exporter.error]);
                break;
                
            case AVAssetExportSessionStatusCancelled:
                
                break;
                
            case AVAssetExportSessionStatusCompleted:
                successCallback(@[@"Merge video complete", myDocumentPath]);
                break;
                
            default:
                break;
        }
    }];
}

/**
 * ExportVideo calls MergeVideo
 */
-(void)ExportVideo:(NSArray *)video destination:(NSString *)destination callback:(RCTResponseSenderBlock)successCallback errorCallback:(RCTResponseSenderBlock)failureCallback {
    
    NSLog(@"destination: %@", destination );
    NSString *music = @"none";
    BOOL overlay = NO;
    if ([destination rangeOfString:@"Watermark"].location == NSNotFound) {
        overlay = YES;
    }
    
    double musicStart = 0;
    
    [self MergeVideo:video music:music musicStart:musicStart showOverlay:&overlay destination:destination callback:successCallback errorCallback:failureCallback];
}

- (NSString*) applicationDocumentsDirectory
{
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

@end

